import { Contact } from "./models/contact";
import { ContactService } from "./services/contact-service";
import { RowHelper } from "./_helpers/row-helper";

document.addEventListener('DOMContentLoaded', (event) => {
    const service = new ContactService()
    const contacts = service.findAll()
    for (let contact of contacts) {
        const row = new RowHelper()

        row.addColumn('&nbsp;').addColumn(contact.getLastName()).addColumn(contact.getFirstName()).addColumn(contact.getOccupation()).addColumn(contact.getCompany()).addColumn('&nbsp')



        document.querySelector('tbody').appendChild(row.buildRow())
    }
    // contact.setLastName('Aubert').setFirstName('Jean-Luc').setOccupation('Teacher').setCompany('Aelion')




    //compteur row

    const nbRows = document.querySelectorAll('tbody tr').length
    console.log(nbRows);
    document.getElementById('items-number').innerText = nbRows

    setTimeout(() => {
        document.querySelector('div.outer-splash').classList.add('hidden')
    }, 1000)
});